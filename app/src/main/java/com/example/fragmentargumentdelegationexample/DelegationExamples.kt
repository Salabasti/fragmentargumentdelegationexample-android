package com.example.fragmentargumentdelegationexample

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class PropertyDelegationExample {
    private val text: String by lazy {
        "This string will be allocated on first read"
    }

    private val otherText = "Will be allocated when class is instantiated."
}


// Class which uses the custom property delegate
class SomeClass {
    var text by StringDelegate()
}

// Delegate which can only be used by SomeClass on properties of type String
class StringDelegate : ReadWriteProperty<SomeClass, String> {

    // Stores the value for the property, just like a regular backing field
    private var stringValue: String = "initial_value"

    // called when property is written
    override fun setValue(thisRef: SomeClass, property: KProperty<*>, value: String) {
        stringValue = value
    }

    // called when property is read
    override fun getValue(thisRef: SomeClass, property: KProperty<*>): String {
        return stringValue
    }
}

class StringDelegateWithoutInterface {

    private var stringValue: String = "initial_value"

    operator fun setValue(thisRef: SomeClass, property: KProperty<*>, value: String) {
        stringValue = value
    }

    operator fun getValue(thisRef: SomeClass, property: KProperty<*>): String {
        return stringValue
    }
}