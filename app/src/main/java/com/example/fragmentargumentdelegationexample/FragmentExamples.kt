package com.example.fragmentargumentdelegationexample

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment


class BadFragment1(
    // will crash, on recreation because no default constructor is defined
    private val textToShow: String
): Fragment()


class BadFragment2: Fragment() {
    // Will be reset to empty string after recreation
    var textToShow: String = ""
}


class GoodFragment: Fragment() {

    // retrieve value from arguments in custom getter
    private val textToShow: String
        get() = requireArguments().getString(ARG_TEXT_TO_SHOW)
            ?: throw IllegalArgumentException("Argument $ARG_TEXT_TO_SHOW required")

    companion object {
        // the name for the argument
        private const val ARG_TEXT_TO_SHOW = "argTextToShow"

        // Use this function to create instances of the fragment
        // and set the passed data as arguments
        fun newInstance(textToShow: String) = GoodFragment().apply {
            arguments = bundleOf(
                ARG_TEXT_TO_SHOW to textToShow
            )
        }
    }
}