package com.example.fragmentargumentdelegationexample

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


/**
 * Will delegate the corresponding property to be stored in the fragments
 * [Fragment.mArguments]. Not all types are supported. Only the types
 * supported by [bundleOf] will work. Otherwise an exception will be thrown.
 * Also make sure to set a value to the property before reading it. Otherwise
 * an exception will be thrown.
 * @throws IllegalArgumentException In case a not supported type is used
 * @throws UninitializedPropertyAccessException If read before value was set.
 */
fun <U : Any?> fragmentArguments() = object : ReadWriteProperty<Fragment, U> {

    override operator fun getValue(
        thisRef: Fragment,
        property: KProperty<*>
    ): U = thisRef.arguments?.get(property.name) as U

    override operator fun setValue(
        thisRef: Fragment,
        property: KProperty<*>,
        value: U
    ) {
        if (thisRef.arguments == null) thisRef.arguments = bundleOf()

        thisRef.requireArguments().putAll(
            bundleOf(property.name to value)
        )
    }
}