package com.example.fragmentargumentdelegationexample

import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.testing.launchFragment
import androidx.fragment.app.testing.withFragment
import junit.framework.TestCase.*
import kotlinx.parcelize.Parcelize
import org.junit.Test

class FragmentArgumentDelegateTest {

    @Test
    fun testArgumentsNull() {
        launchFragment<StringPropertyFragment>().withFragment {
            assertNull(arguments)
        }
    }

    @Test(expected = UninitializedPropertyAccessException::class)
    fun testThrowsException() {
        launchFragment<StringPropertyFragment>().withFragment {
            text
        }
    }

    @Test
    fun testPropertySetGet() {
        launchFragment<StringPropertyFragment>().withFragment {
            text = "test"
            assertEquals("test", text)
        }
    }

    @Test
    fun testNullable() {
        launchFragment<NullableStringPropertyFragment>().withFragment {
            text = null
            assertNotNull(arguments)
            assertNull(text)

            text = "test"
            assertEquals("test", text)
        }
    }

    @Test
    fun testRecreation() {
        val scenario = launchFragment<StringPropertyFragment>()
        scenario.withFragment {
            text = "test"
        }
        scenario.recreate()
        scenario.withFragment {
            assertEquals("test", text)
        }
    }

    @Test
    fun testNumberProperty() {
        launchFragment<IntPropertyFragment>().withFragment {
            number = 99
            assertEquals(99, number)
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun testCustomType() {
        launchFragment<ExampleTypePropertyFragment>().withFragment {
            example = ExampleType()
        }
    }

    @Test
    fun testParcelable() {
        launchFragment<ExampleParcelablePropertyFragment>().withFragment {
            parcelable = ExampleParcelable(value = "test")
            assertEquals("test", parcelable.value)
        }
    }

    @Test
    fun testEnum() {
        launchFragment<ExampleEnumPropertyFragment>().withFragment {
            enum = ExampleEnum.Foo
            assertEquals(ExampleEnum.Foo, enum)
        }
    }
}


class StringPropertyFragment : Fragment() {
    var text: String by fragmentArguments()
}


class NullableStringPropertyFragment : Fragment() {
    var text: String? by fragmentArguments()
}


class IntPropertyFragment : Fragment() {
    var number: Int by fragmentArguments()
}


class ExampleType


class ExampleTypePropertyFragment: Fragment() {
    var example: ExampleType by fragmentArguments()
}


@Parcelize
data class ExampleParcelable(
    val value: String
) : Parcelable


class ExampleParcelablePropertyFragment: Fragment() {
    var parcelable: ExampleParcelable by fragmentArguments()
}


enum class ExampleEnum {
    Foo
}


class ExampleEnumPropertyFragment: Fragment() {
    var enum: ExampleEnum by fragmentArguments()
}


